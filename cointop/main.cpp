#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include "Matrix.h"
#include "binomial_test.h"
#include "test_cases.h"


namespace EOptions
{
    const int NONE                  = 0;
    const int MAX_MODE_ACCOMPANY    = 1;
    const int MAX_MODE_AVOID        = 2;
    const int MAX_MODE_MASK         = 1 | 2;
    const int ALT_HYP_LESS          = 4;
    const int ALT_HYP_GREATER       = 8;
    const int ALT_HYP_TWOTAILED     = 16;
    const int ALT_HYP_MASK          = 4 | 8 | 16;
    const int SET_MODE_FULL         = 32;
    const int SET_MODE_INTERSECTION = 64;
    const int SET_MODE_MASK         = 32 | 64;
    const int SIG_COR_NONE          = 128;
    const int SIG_COR_BON           = 256;
    const int SIG_COR_MASK          = 128 | 256;
    const int VERBOSE               = 512;
};

typedef int EOptionsT;


void write_header();
char* read_all_bytes( const std::string& filename );
std::map<int, std::string>* read_names( const std::string& file_name );
void coincidence_to_p( const std::map<int, std::string>& col_names, const Matrix& matrix, const int i, const int j, const int max_coincidence, const EOptionsT options, const double sig_level, int& sig_index );
void calculate( const std::map<int, std::string>& col_names, const Matrix& matrix, int max_coincidence, double sig_level, EOptionsT options );
int read_summary( const std::string& file_name );

double correct_sig( const Matrix& matrix, double level, EOptionsT type );


/**
 * Interprets the flags part of the command line
 * 
 * @param flags_name    Flags as provided by user 
 * @return Enumeration value 
 */
EOptionsT read_flags( const std::string& flags_name )
{
    EOptionsT flags = EOptions::NONE;

    for (const char& f : flags_name)
    {
        switch (f)
        {
            case 'o':
                flags &= ~EOptions::MAX_MODE_MASK;
                flags |= EOptions::MAX_MODE_ACCOMPANY;
                break;
            case 's':
                flags &= ~EOptions::MAX_MODE_MASK;
                flags |= EOptions::MAX_MODE_AVOID;
                break;
            case 'l':
                flags &= ~EOptions::ALT_HYP_MASK;
                flags |= EOptions::ALT_HYP_LESS;
                break;
            case 'g':
                flags &= ~EOptions::ALT_HYP_MASK;
                flags |= EOptions::ALT_HYP_GREATER;
                break;
            case 't':
                flags &= ~EOptions::ALT_HYP_MASK;
                flags |= EOptions::ALT_HYP_TWOTAILED;
                break;
            case 'f':
                flags &= ~EOptions::SET_MODE_MASK;
                flags |= EOptions::SET_MODE_FULL;
                break;
            case 'i':
                flags &= ~EOptions::SET_MODE_MASK;
                flags |= EOptions::SET_MODE_INTERSECTION;
                break;
            case 'n':
                flags &= ~EOptions::SIG_COR_MASK;
                flags |= EOptions::SIG_COR_NONE;
                break;
            case 'b':
                flags &= ~EOptions::SIG_COR_MASK;
                flags |= EOptions::SIG_COR_BON;
                break;
            case 'v':
                flags |= EOptions::VERBOSE;
                break;
            case '1':
                flags = EOptions::MAX_MODE_ACCOMPANY | EOptions::SET_MODE_FULL | EOptions::ALT_HYP_GREATER | EOptions::SIG_COR_BON;
                break;
            case '0':
                flags = EOptions::MAX_MODE_AVOID | EOptions::SET_MODE_INTERSECTION | EOptions::ALT_HYP_GREATER | EOptions::SIG_COR_BON;
                break;
            case 'd':
                run_tests();
                break;
            default:
                std::stringstream ss;
                ss << "Invalid flag '" << f << "' specified in command line.";
                throw std::logic_error( ss.str().c_str() );
        }
    }

    if (flags & EOptions::ALT_HYP_GREATER)
    {
        std::cerr << "ALT_HYP.... = GREATER" << std::endl;
    }

    if (flags & EOptions::ALT_HYP_LESS)
    {
        std::cerr << "ALT_HYP.... = LESS" << std::endl;
    }

    if (flags & EOptions::ALT_HYP_TWOTAILED)
    {
        std::cerr << "ALT_HYP.... = TWOTAILED" << std::endl;
    }

    if (flags & EOptions::SET_MODE_FULL)
    {
        std::cerr << "SET_MODE... = FULL" << std::endl;
    }

    if (flags & EOptions::SET_MODE_INTERSECTION)
    {
        std::cerr << "SET_MODE... = INTERSECTION" << std::endl;
    }

    if (flags & EOptions::MAX_MODE_ACCOMPANY)
    {
        std::cerr << "MAX_MODE... = ACCOMPANY" << std::endl;
    }

    if (flags & EOptions::MAX_MODE_AVOID)
    {
        std::cerr << "MAX_MODE... = AVOID" << std::endl;
    }

    if (flags & EOptions::SIG_COR_BON)
    {
        std::cerr << "SIG_COR.... = BONFERRONI" << std::endl;
    }

    if (flags & EOptions::SIG_COR_NONE)
    {
        std::cerr << "SIG_COR.... = NONE" << std::endl;
    }

    if (flags & EOptions::VERBOSE)
    {
        std::cerr << "VERBOSE.... = TRUE" << std::endl;
    }
    else
    {
        std::cerr << "VERBOSE.... = FALSE" << std::endl;
    }

    return flags;
}


/**
 * Entry point
 * 
 * @param argc  System
 * @param argv  System
 * @return System 
 */
int main( int argc, const char** argv )
{
    if (argc != 4)
    {
        std::cerr << "Invalid arguments, please consult the readme." << std::endl;
        std::cerr << "Usage: cointop <matrix> <sig-level> <flags>" << std::endl;
        return 1;
    }

    std::string file_name_prefix       = argv[ 1 ];
    std::string coin_matrix_file_name  = file_name_prefix + "-matrix.bin";
    std::string coin_lookup_file_name  = file_name_prefix + "-colnames.tsv";
    std::string coin_summary_file_name = file_name_prefix + "-summary.tsv";
    double      sig_level              = std::stod( argv[ 2 ] );
    std::string flags_name             = argv[ 3 ];

    std::cerr << "IN-MATRIX.. = " << coin_matrix_file_name << std::endl;
    std::cerr << "IN-COLNAMES = " << coin_lookup_file_name << std::endl;
    std::cerr << "IN-SUMMARY. = " << coin_summary_file_name << std::endl;
    std::cerr << "SIG-LEVEL.. = " << sig_level << std::endl;
    std::cerr << "OUTPUT-TO.. = STDOUT" << std::endl;
    EOptionsT flags = read_flags( flags_name );


    std::map<int, std::string>* name_lookup = read_names( coin_lookup_file_name );
    char                      * buffer      = read_all_bytes( coin_matrix_file_name );
    int max_coincidence = read_summary( coin_summary_file_name );
    Matrix* matrix = new Matrix( reinterpret_cast<int*>(buffer), static_cast<int>(name_lookup->size()));
    sig_level = correct_sig( *matrix, sig_level, flags );
    calculate( *name_lookup, *matrix, max_coincidence, sig_level, flags );

    std::cerr << "Cleaning up..." << std::endl;
    delete matrix;
    delete[] buffer;
    delete name_lookup;

    std::cerr << "All done!" << std::endl;
    return 0;
}


int read_summary( const std::string& file_name )
{
    std::cerr << "Reading summary..." << std::endl;

    std::ifstream file_in;
    file_in.open( file_name );
    std::string cell;
    bool        left      = true;
    std::string key;
    int         num_betas = -1;

    while (getline( file_in, cell, left ? static_cast<char>('\t') : static_cast<char>('\n')))
    {
        if (left)
        {
            key = cell;
        }
        else
        {
            if (key == "num_beta")
            {
                num_betas = std::stoi( cell );
            }
        }

        left = !left;
    }

    if (num_betas == -1)
    {
        throw std::logic_error( "num_beta not found in summary file." );
    }

    return num_betas;
}


/**
 * Runs the calculations, outputting significant values
 * 
 * @param matrix            Coincidence matrix 
 * @param max_coincidence   Max coincidence level (number of beta) 
 * @param sig_level         Significance level (corrected)
 * @param options           Run flags
 */
void calculate( const std::map<int, std::string>& col_names, const Matrix& matrix, int max_coincidence, double sig_level, EOptionsT options )
{
    if (options & EOptions::VERBOSE)
    {
        std::cerr << "Verbose mode is active. Expect lots of output." << std::endl;
    }

    std::cerr << "Writing data..." << std::endl;
    write_header();
    int sig_index;

    int n = matrix.get_num_cols();

    for (int i = 0; i < n; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            coincidence_to_p( col_names, matrix, i, j, max_coincidence, options, sig_level, sig_index );
        }
    }
}


/**
 * Corrects the significance level
 * @param matrix 
 * @param sig_level 
 * @param options 
 * @return 
 */
double correct_sig( const Matrix& matrix, double sig_level, EOptionsT options )
{
    int n             = matrix.get_num_cols();
    int triangle_size = ( n * ( n - 1 )) / 2;
    std::cerr << "The upper triangle of the " << n << "x" << n << " matrix is " << triangle_size << "." << std::endl;

    switch (options & EOptions::SIG_COR_MASK)
    {
        case EOptions::SIG_COR_NONE:
            std::cerr << "No correction, the significance level is " << sig_level << "." << std::endl;
            return sig_level;
        case EOptions::SIG_COR_BON:
            std::cerr << "The Bonferroni correction reduces the significance level from " << sig_level << " to " << ( sig_level / triangle_size ) << "." << std::endl;
            return sig_level / triangle_size;
        default:
            throw std::logic_error( "Invalid options around SET_MODE_MASK." );
    }
}


/**
 * Calculates a p-value.
 */
void coincidence_to_p( const std::map<int, std::string>& col_names, const Matrix& matrix, const int i, const int j, const int max_coincidence, const EOptionsT options, const double sig_level, int& sig_index )
{
    int num_observations;
    
    int one_of  = matrix.get_one_of( i, j );
    int both_of = matrix.get_both_of( i, j );

    switch (options & EOptions::SET_MODE_MASK)
    {
        case EOptions::SET_MODE_INTERSECTION:
            num_observations = one_of;

            if (num_observations == 0)
            {
                if (options & EOptions::VERBOSE)
                {
                    std::cerr << "Rejected (" << i << ", " << j << ") because there are no observations." << std::endl;
                }
                return;
            }
            break;

        case EOptions::SET_MODE_FULL:
            num_observations = max_coincidence;
            break;

        default:
            throw std::logic_error( "Invalid options around SET_MODE_MASK." );
    }

    //  Get the amount of times each ALPHA showed up in ANY other BETA
    int any_i = matrix.get_total_sum( i );
    int any_j = matrix.get_total_sum( j );

    // From this we can work out the chance of alpha/beta occurring at the same time
    double chance_i = static_cast<double>(any_i) / static_cast<double>(num_observations);
    double chance_j = static_cast<double>(any_j) / static_cast<double>(num_observations);
    
    double not_cross_1_chance = static_cast<double>( num_observations - any_i ) / static_cast<double>(num_observations);
    double not_cross_2_chance = static_cast<double>( num_observations - any_j ) / static_cast<double>(num_observations);
    
    double rate;
    int    successes;

    switch (options & EOptions::MAX_MODE_MASK)
    {
        case EOptions::MAX_MODE_AVOID:
        {
            successes = one_of - both_of;   // note that the upper triangle (i,j) is 1&2 whilst the lower triangle (j,i) is 1|2
            rate      = ( chance_i * not_cross_2_chance ) + ( chance_j * not_cross_1_chance );
            break;
        }
        case EOptions::MAX_MODE_ACCOMPANY:
        {
            successes = both_of;
            rate      = chance_i * chance_j;
            break;
        }
        default:
        {
            throw std::logic_error( "Invalid options around MAX_MODE_MASK." );
        }
    }

    // This causes problems, get rid of it
    if (rate == 0 or rate == 1.0)
    {
        if (options & EOptions::VERBOSE)
        {
            std::cerr << "Rejected (" << i << ", " << j << ") because the rate is " << rate << "." << std::endl;
        }
        return;
    }

    // Binomial test p-value
    double p_value;

    switch (options & EOptions::ALT_HYP_MASK)
    {
        case EOptions::ALT_HYP_TWOTAILED:
            p_value = binom_2sided( static_cast<uint32_t>(successes), static_cast<uint32_t>(num_observations), rate, 0 );
            break;

        case EOptions::ALT_HYP_LESS:
            p_value = binom_1sided_less( successes, num_observations, rate );
            break;

        case EOptions::ALT_HYP_GREATER:
            p_value = binom_1sided_greater( successes, num_observations, rate );
            break;

        default:
            throw std::logic_error( "Invalid options around ALT_HYP_MASK." );
    }
    
    if (options & EOptions::VERBOSE)
    {
        std::cerr << "*******************************" << std::endl;
        std::cerr << "* i                   " << i << "." << std::endl;
        std::cerr << "* j                   " << j << "." << std::endl;
        std::cerr << "*------------------------------" << std::endl;
        std::cerr << "* any_i               " << any_i << "." << std::endl;
        std::cerr << "* any_j               " << any_j << "." << std::endl;
        std::cerr << "* both_of             " << both_of << "." << std::endl;
        std::cerr << "* one_of              " << one_of << "." << std::endl;
        std::cerr << "* max_coincidence     " << max_coincidence << "." << std::endl;
        std::cerr << "*------------------------------" << std::endl;
        
        std::cerr << "*------------------------------" << std::endl;
        std::cerr << "* chance_i            " << chance_i << "." << std::endl;
        std::cerr << "* chance_j            " << chance_j << "." << std::endl;
        std::cerr << "* not_cross_1_chance  " << not_cross_1_chance << "." << std::endl;
        std::cerr << "* not_cross_2_chance  " << not_cross_2_chance << "." << std::endl;
        std::cerr << "*------------------------------" << std::endl;
        std::cerr << "* rate                " << rate << "." << std::endl;
        std::cerr << "* successes           " << successes << "." << std::endl;
        std::cerr << "* num_observations    " << num_observations << "." << std::endl;
        std::cerr << "*------------------------------" << std::endl;
        std::cerr << "* p_value LESS        " << binom_1sided_less( successes, num_observations, rate ) << "." << std::endl;
        std::cerr << "* p_value GREATER     " << binom_1sided_greater( successes, num_observations, rate ) << "." << std::endl;
        std::cerr << "* p_value TWOTAILED   " << binom_2sided( static_cast<uint32_t>(successes), static_cast<uint32_t>(num_observations), rate, 0 ) << "." << std::endl;
        std::cerr << "*******************************" << std::endl;
    }

    if (p_value > sig_level)
    {
        if (options & EOptions::VERBOSE)
        {
            std::cerr << "Rejected (" << i << ", " << j << ") because it isn't significant with p = " << p_value << "." << std::endl;
        }
        return;
    }

    if (options & EOptions::VERBOSE)
    {
        std::cerr << "Accepted (" << i << ", " << j << ") because it is significant with p = " << p_value << "." << std::endl;
    }

    ++sig_index;

    std::cout << "S_" << sig_index
              << "," << i
              << "," << j
              << "," << col_names.at( i )
              << "," << col_names.at( j )
              << "," << p_value
              << "," << successes
              << "," << num_observations
              << "," << rate
              << "," << static_cast<int>(rate * num_observations + 0.5)
              << "," << any_i
              << "," << any_j
              << "," << chance_i
              << "," << chance_j
              << std::endl;
}


void write_header()
{
    std::cout << ""
              << "," << "Source"
              << "," << "Target"
              << "," << "name_i"
              << "," << "name_j"
              << "," << "p"
              << "," << "suc"
              << "," << "obs"
              << "," << "rate"
              << "," << "exp"
              << "," << "tot_i"
              << "," << "tot_j"
              << "," << "frac_i"
              << "," << "frac_j"
              << std::endl;
}


char* read_all_bytes( const std::string& filename )
{
    std::cerr << "Reading table..." << std::endl;

    std::ifstream           ifs( filename, std::ios::binary | std::ios::ate );
    std::ifstream::pos_type length = ifs.tellg();

    char* buffer = new char[length];
    ifs.seekg( 0, std::ios::beg );
    ifs.read( buffer, length );

    ifs.close();
    return buffer;
}


std::map<int, std::string>* read_names( const std::string& file_name )
{
    std::cerr << "Reading names..." << std::endl;

    std::ifstream file_in;
    file_in.open( file_name );
    std::string cell;
    bool        left = true;
    std::string name;
    std::map<int, std::string>* name_lookup = new std::map<int, std::string>();

    while (getline( file_in, cell, left ? static_cast<char>('\t') : static_cast<char>('\n')))
    {
        if (left)
        {
            name = cell;
        }
        else
        {
            int index = std::stoi( cell );
            ( *name_lookup )[ index ] = name;
        }

        left = !left;
    }

    return name_lookup;
}