//
// Created by Martin Rusilowicz on 21/08/2017.
//

#include <iostream>
#include "test_cases.h"
#include "binomial_test.h"


void test( int suc, int obs, double rate, double two_tailed_expected, double less_than_expected, double greater_than_expected )
{
    double two_tailed_result = binom_2sided( suc, obs, rate, 0 );
    double greater_than_result = binom_1sided_greater( suc, obs, rate );
    double less_than_result = binom_1sided_less( suc, obs, rate );
    
    double dev_tt = two_tailed_result / two_tailed_expected;
    double dev_lt = less_than_expected / less_than_expected;
    double dev_gt = greater_than_result / greater_than_expected;

    std::cerr << "----------------------------" << std::endl;
    std::cerr << "- SUCCESSES = " << suc << ", OBSERVATIONS = " << obs << ", RATE = " << rate << std::endl;
    std::cerr << "- TWO-TAILED   = " << two_tailed_result << " (EXPECTED = " << two_tailed_expected << ")" << " DIFF = " << dev_tt <<  std::endl;
    std::cerr << "- LESS-THAN    = " << less_than_result << " (EXPECTED = " << less_than_expected << ")" << " DIFF = " << dev_lt << std::endl;
    std::cerr << "- GREATER-THAN = " << greater_than_result << " (EXPECTED = " << greater_than_expected << ")" << " DIFF = " << dev_gt << std::endl;
    std::cerr << "----------------------------" << std::endl;
}


void run_tests()
{
    std::cerr << "TEST CASES" << std::endl;

    test(5, 10, 0.5, 1, 0.623046875, 0.623046875);
    test(2, 10, 0.01, 0.00426620024283143, 0.999886150882094, 0.00426620024283143);
    test(5, 100, 0.5, 1.25232451253854e-22, 6.2616225626927e-23, 1);
    test(2, 100, 0.01, 0.264238021077044, 0.92062679774782, 0.264238021077044);
    test(94, 111, 0.845641576917842, 1, 0.554308921570432, 0.550241728270883);
    test(61, 83, 0.0635906057432294, 1.69678999205092e-54, 1, 1.69678999205092e-54);
    test(92, 123, 0.000664655119180679, 5.55478089008345e-264, 1, 5.55478089008345e-264);
    test(70, 107, 0.602323648519814, 0.323269743229878, 0.884687057214992, 0.159189057780731);
    test(23, 86, 0.0422850146424025, 8.86831680671257e-13, 0.999999999999898, 8.86831680671257e-13);
    test(86, 91, 0.0484641883522272, 3.21435455490578e-106, 1, 3.21435455490578e-106);
    test(90, 138, 0.192549366503954, 6.0615009543517e-32, 1, 6.0615009543517e-32);
    test(21, 59, 0.9021095498465, 2.81686376001853e-24, 2.81686376001853e-24, 1);
    test(16, 115, 0.775816608453169, 1.38639636569433e-47, 1.38639636569433e-47, 1);
    test(100, 141, 0.350357226561755, 5.59324481651797e-18, 1, 4.57980908588393e-18);
    test(42, 45, 0.36826142296195, 2.23833125936789e-15, 1, 2.23833125936789e-15);
    test(75, 78, 0.537185027031228, 5.07586479355905e-17, 1, 4.53732235905248e-17);
    test(44, 61, 0.936377916950732, 1.63188077045283e-07, 1.63188077045283e-07, 0.999999973277157);
    test(69, 132, 0.560099303489551, 0.430167659655788, 0.218123142466746, 0.829698516620141);
    test(12, 12, 0.555, 0.0009144143,1,0.0008541139);
    

    exit( 0 );
}