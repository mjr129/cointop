# Coin(cidence) to p(-value)

Converts the coincidence matrix to p-values and outputs the significant elements.

***NOTICE: This application is obsolete, the previous stage of the workflow "`coinfinder`" now incorporates this stage into its own analysis.***

## Usage

`cointop <matrix> <lookup> <siglevel> <flags>`

* `<infiles>` coincidence file prefix as specified as the output when you ran `coinfinder.exe`
* `<siglevel>` significance level
* `<flags>` run flags, one of each group:
    * coincidence mode:
        * `o` accompany (overlap)
        * `s` avoid (separate)
    * set mode:
        * `f` full
        * `i` intersection
    * alt-hypothesis:
        * `l` less-than
        * `g` greater-than
        * `t` two-tailed
    * significance correction:
        * `n` none
        * `b` Bonferroni (harsh)
    * miscellaneous:
        * `v` verbose
    * or:
        * `1` synonym for `ofgb`
        * `0` synonym for `sigb`
    * later flags will override earlier ones, hence `fi` = `f` and `1r` = `cfgr`.
* `stderr` all informative messages are sent to std.error.
* `stdout` the final CSV is sent to std.out.
 
## example

`cointop plasmids 0.05 1`

## Output

The output is a CSV, with the following columns.

0. `(no-name)` - Row name (arbitrary)
1. `Source` - Index of first alpha group*
1. `Target` - Index of second alpha group*
1. `name_i` - Name of first alpha group
2. `name_j` - Name of second alpha group
3. `p` - p-value on overlap/avoidance between groups
4. `suc` - number of overlaps/avoidances between groups
5. `obs` - number of observations (chances to overlap/avoid)
6. `rate` - expected rate of overlap/avoidance
7. `exp` - expected number of overlaps/avoidances (approximate)
8. `tot_i` - number of observations of first alpha group
9. `tot_j` - number of observations of second alpha group
10. `frac_i` - fraction of observations in first alpha group
11. `frac_j` - fraction of observations in second alpha group

*These fields are named `Source` and `Target` instead of `index_i` and `index_j` to allow importation into ***Gephi***.

## Meta

```
author: Martin Rusilowicz
license: GPLv3
uses-code-from: Chang, C. (2014) Binomial test. URL https://github.com/chrchang/stats. Accessed 2017-08-18.
tags: coincidence, accompany, avoid
workflow-previous: coinfinder
```

