#ifndef COIN_TO_P_BINOMIAL_TEST_H
#define COIN_TO_P_BINOMIAL_TEST_H

#include <stdint.h>

double binom_2sided(uint32_t succ, uint32_t obs, double rate, uint32_t midp);
double binom_1sided_greater(int succ, int obs, double rate);
double binom_1sided_less(int succ, int obs, double rate);

#endif //COIN_TO_P_BINOMIAL_TEST_H
