//
// Created by Martin Rusilowicz on 17/08/2017.
//

#ifndef COIN_TO_P_MATRIX_H
#define COIN_TO_P_MATRIX_H


class Matrix
{
    private:
        int* _buffer;
        int _num_cols;

    public:
        Matrix(int* buffer, int num_cols);
        int get_num_cols() const;
        int get_total_sum(int i) const;
        int get_one_of(int i, int j) const;
        int get_both_of(int i, int j) const;
        
    private:
        int get_cell(int row, int col) const;
};


#endif //COIN_TO_P_MATRIX_H
