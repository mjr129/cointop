//
// Created by Martin Rusilowicz on 17/08/2017.
//

#include <stdexcept>
#include "Matrix.h"


Matrix::Matrix( int* buffer, int num_cols )
:_buffer(buffer)
,_num_cols(num_cols)
{
}


int Matrix::get_num_cols() const
{
    return this->_num_cols;
}


int Matrix::get_total_sum( int i ) const
{
    return get_cell(i, i);
}


int Matrix::get_cell( int row, int col ) const
{
    return _buffer[col + row * _num_cols];
}


int Matrix::get_one_of( int i, int j ) const
{
    if (j <= i)
    {
        throw std::logic_error("j < i");
    }
    
    return get_cell(j, i);
}


int Matrix::get_both_of( int i, int j ) const
{
    if (j <= i)
    {
        throw std::logic_error("j < i");
    }
    
    return get_cell(i, j);
}
